import os
from flask import Flask
from os import path
from dotenv import load_dotenv
from src.services import bpAuth, bpFile, bpEmail




app = Flask(__name__, instance_relative_config=True)
app.register_blueprint(bpAuth)
app.register_blueprint(bpFile)
app.register_blueprint(bpEmail)


# app.config["UPLOAD_FOLDER"] = os.environ.get("UPLOAD_FOLDER_PUBLIC")


#import .env
# basedir = path.abspath(path.dirname(__file__))
# load_dotenv(path.join(basedir, ".env"))
#end of line import .env


# api = Api(app)

# #Area of Class Logic function    
# class CredentialFunction():
#     def __init__(self, args):
#         constants.EMAIL_OUTLOOK=args['email']
#         constants.PASSWORD_OUTLOOK=args['password']
    
#     def getCredential(self):
#         response = {'emailOutlook': constants.EMAIL_OUTLOOK, 'passwordOutlook':constants.PASSWORD_OUTLOOK}
#         return response
#         # return jsonify(emailOutlook=self.email, passwordOutlook=self.password)
    
# class File():
#     def allow_file_upload(filename):
#         path_file = "public/file/"
#         allow_ext = {'xlsx', 'xls', 'csv', 'pdf'}
#         app.config['UPLOAD_FOLDER'] = path_file
#         return '.' in filename and \
#            filename.rsplit('.', 1)[1].lower() in allow_ext
#     def checkFileExists(self, param):
#         path = 'public/file/'+str(param['filename'])
#         if os.path.exists(path):
#             return HTTPStatus.ACCEPTED
#         else :
#             return HTTPStatus.NOT_FOUND
        
# class Email():
#     def __init__(self, args):
#         self.args = args
#     # def StartBulkEmail(self):
#     #     initFile = File()
#     #     check_file = initFile.checkFileExists(self.args)
#     #     # return jsonify({"test":args["filename"]})
#     #     if check_file == 202:
#     #         initLoadDataset = dataset()
#     #         loadData = initLoadDataset.loaddata(self.args)
#     #         sentEmail = asyncio.run(self.BulkSentEmail(loadData))
#     #         response = jsonify({"Total Row Success":constants.SENT_SUCCESS_EMAIL,"Total Row Error":constants.SENT_ERROR_EMAIL,"message":sentEmail, "status_code":HTTPStatus.ACCEPTED})
#     #         # response = {"Total Row Success":constants.SENT_SUCCESS_EMAIL,"Total Row Error":constants.SENT_ERROR_EMAIL, "status_code":HTTPStatus.ACCEPTED}
#     #     else:
#     #         response = jsonify({"message":"File Doesn't Exists", "status_code":check_file})
#     #     return response
#     async def BulkSentEmail(self):
#         initFile = File()
#         check_file = initFile.checkFileExists(self.args)
#         await asyncio.sleep(0.2)
#         # return jsonify({"name":self.args["filename"]})
#         if check_file == 202:
#             initLoadDataset = dataset()
#             loadData = initLoadDataset.loaddata(self.args)
#             username = constants.EMAIL_OUTLOOK
#             password = constants.PASSWORD_OUTLOOK
#             smtp_server = 'webmail.bankmandiri.co.id'
#             # smtp_server = "10.204.97.199"
#             smtp_port = 25
#             cc_recipient = 'it-csm-csi@bankmandiri.co.id'
#             ErrEmail = []
#             coloumn = ["ErrorEmail"]
#             for index, row in loadData.iterrows():
#                 message = MIMEMultipart()
#                 message['From'] = username
#                 message['To'] = row["emailPenerima"]
#                 message['Cc'] = ', '.join(cc_recipient)
#                 message['Subject'] = 'Testing Email'
            
#                 # html_part = MIMEText(self.HTMLTemplate, 'html')
#                 message.attach(MIMEText("Test", 'plain'))
#                 try:
#                     with smtplib.SMTP_SSL(smtp_server, smtp_port) as server:
#                         server.login(username, password)
#                         server.send_message(message)
#                         constants.SENT_SUCCESS_EMAIL += 1
#                 except Exception as e:
#                     constants.SENT_ERROR_EMAIL += 1
#                     ErrEmail.append(row["emailPenerima"])
#                     constants.RESPONSE_MESSAGE = str(e)
#                     continue
#             errDF = dataset.createDF(ErrEmail, coloumn)
#             dataset.insertErrID(errDF, constants.FILE_UPLOAD_NAME)
#             response = jsonify({"Total Row Success":constants.SENT_SUCCESS_EMAIL,"Total Row Error":constants.SENT_ERROR_EMAIL,"message":constants.RESPONSE_MESSAGE, "status_code":HTTPStatus.ACCEPTED})
#         else:
#             response = jsonify({"message":"File Doesn't Exists", "status_code":HTTPStatus.NOT_FOUND})
#         await asyncio.sleep(0.2)
#         return response
#     def HTMLTemplate():
#         html_template = """
#             <html>
#                 <head></head>
#                     <body>
#                         <h1>Ini adalah template HTML untuk email</h1>
#                         <p>Halo, Ini adalah isi email dengan menggunakan HTML.</p>
#                         <p>Terima kasih.</p>
#                     </body>
#             </html>
#             """
#         return html_template
        
# class dataset():
#     def loaddata(self, args):
#         file_path = "public/file/"+args['filename']
#         file_path = os.path.abspath(file_path)
#         df = pd.read_excel(file_path, sheet_name=0)
#         df = pd.DataFrame(df)
#         return df
#     def createDF(arry, colomn):
#         columns=colomn
#         df = pd.DataFrame(list(zip(arry)), columns=columns)
#         return df
#     def insertErrID(df, filename):
#         file_path = "public/file/"+"Err"+filename
#         file_path = os.path.abspath(file_path)
#         df.to_excel(file_path, index=False, sheet_name='errEmail')
#  #End Of Line Class Logic Function


@app.route("/", methods=['GET'])
def index():
    return "Hello"

# @app.route('/file', methods=['POST'])
# def upload_file():
#     # check if the post request has the file part
# 	if 'filename' not in request.files:
# 		resp = jsonify({'message' : 'No file part in the request'})
# 		resp.status_code = 400
# 		return resp
# 	file = request.files['filename']
# 	if file.filename == '':
# 		resp = jsonify({'message' : 'No file selected for uploading'})
# 		resp.status_code = 400
# 		return resp
# 	if file and File.allow_file_upload(file.filename):
# 		filename = secure_filename(file.filename)
# 		file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
# 		resp = jsonify({'message' : 'File successfully uploaded'})
# 		resp.status_code = 201
# 		return resp
# 	else:
# 		resp = jsonify({'message' : 'Allowed file types are txt, pdf, png, jpg, jpeg, gif'})
# 		resp.status_code = 400
# 		return resp

# @app.route('/email/Auth', methods=['POST'])
# def Auth365post():
#         parser = reqparse.RequestParser()
#         parser.add_argument('email', required=True)
#         parser.add_argument('password', required=True)
        
#         args = parser.parse_args()
#         initLogin = CredentialFunction(args)
#         getCreden = initLogin.getCredential()
#         return getCreden, HTTPStatus.ACCEPTED


#Line For Class Flask Restful
# class Auth365(Resource):
#     def post(self):
#         parser = reqparse.RequestParser()
#         parser.add_argument('email', required=True)
#         parser.add_argument('password', required=True)
        
#         args = parser.parse_args()
#         initLogin = CredentialFunction(args)
#         getCreden = initLogin.getCredential()
#         return getCreden, HTTPStatus.ACCEPTED

# class AutomationEmail(Resource):
#     def post(self):
#         parser = reqparse.RequestParser()
#         parser.add_argument('filename', required=True)
#         args = parser.parse_args()
#         initEmail = Email(args)
#         constants.FILE_UPLOAD_NAME=args["filename"]
#         check_file = asyncio.run(initEmail.BulkSentEmail())
#         return make_response(check_file)
# api.add_resource(Auth365, '/email/Auth')
# api.add_resource(AutomationEmail, '/email/bulksent/')

# End of line Class Flask Restful