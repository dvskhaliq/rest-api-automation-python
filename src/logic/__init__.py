from src.constants import constants
from http import HTTPStatus
import os
import asyncio 
from flask import jsonify
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import pandas as pd
import logging

logging.basicConfig(filename="src/logs/process.txt", level=logging.DEBUG)
#Area of Class Logic function    
class CredentialFunction():
    def __init__(self, args):
        constants.USERNAME_OUTLOOK=args['username']
        constants.PASSWORD_OUTLOOK=args['password']
        constants.EMAIL_OUTLOOK = args['email']
    
    def getCredential(self):
        response = {'emailOutlook': constants.EMAIL_OUTLOOK, 'passwordOutlook':constants.PASSWORD_OUTLOOK}
        return response
        # return jsonify(emailOutlook=self.email, passwordOutlook=self.password)
    
class File():
    def allow_file_upload(filename):
        # path_file = "public/file/"
        allow_ext = {'xlsx', 'xls', 'csv', 'pdf'}
        return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in allow_ext
    def checkFileExists(self, param):
        path = constants.PUBLIC_DIR+'/files/'+str(param['filename'])
        if os.path.exists(path):
            return HTTPStatus.ACCEPTED
        else :
            return HTTPStatus.NOT_FOUND
        
class Email():
    def __init__(self, args):
        self.args = args
    # def StartBulkEmail(self):
    #     initFile = File()
    #     check_file = initFile.checkFileExists(self.args)
    #     # return jsonify({"test":args["filename"]})
    #     if check_file == 202:
    #         initLoadDataset = dataset()
    #         loadData = initLoadDataset.loaddata(self.args)
    #         sentEmail = asyncio.run(self.BulkSentEmail(loadData))
    #         response = jsonify({"Total Row Success":constants.SENT_SUCCESS_EMAIL,"Total Row Error":constants.SENT_ERROR_EMAIL,"message":sentEmail, "status_code":HTTPStatus.ACCEPTED})
    #         # response = {"Total Row Success":constants.SENT_SUCCESS_EMAIL,"Total Row Error":constants.SENT_ERROR_EMAIL, "status_code":HTTPStatus.ACCEPTED}
    #     else:
    #         response = jsonify({"message":"File Doesn't Exists", "status_code":check_file})
    #     return response
    async def BulkSentEmail(self):
        initFile = File()
        check_file = initFile.checkFileExists(self.args)
        # await asyncio.sleep(0.2)
        # return jsonify({"name":self.args["filename"]})
        if check_file == 202:
            initLoadDataset = dataset()
            loadData = initLoadDataset.loaddata(self.args)
            username = constants.USERNAME_OUTLOOK
            password = constants.PASSWORD_OUTLOOK
            email = constants.EMAIL_OUTLOOK
            # smtp_server = 'relay.corp.bankmandiri.co.id'
            # smtp_port = 25
            smtp_server = 'webmail.bankmandiri.co.id'
            # smtp_port = 25
            cc_recipient = 'it-csm-csi@bankmandiri.co.id'
            ErrEmail = []
            coloumn = ["ErrorEmail"]
            for index, row in loadData.iterrows():
                message = MIMEMultipart()
                message['From'] = email
                message['To'] = row["emailPenerima"]
                message['Cc'] = ', '.join(cc_recipient)
                message['Subject'] = 'Testing Email'
            
                # html_part = MIMEText(self.HTMLTemplate, 'html')
                message.attach(MIMEText("Test", 'plain'))
                try:
                    with smtplib.SMTP(smtp_server) as server:
                        server.login(username, password)
                        server.send_message(message)
                        constants.SENT_SUCCESS_EMAIL += 1
                except Exception as e:
                    constants.SENT_ERROR_EMAIL += 1
                    ErrEmail.append(row["emailPenerima"])
                    constants.RESPONSE_MESSAGE = str(e)
                    message  = "\n=========================================================\n"
                    message += "         "+e+str(row['emailPenerima'])+"          \n"
                    message += "=========================================================\n"
                    logging.warning(message)
                    continue
            errDF = dataset.createDF(ErrEmail, coloumn)
            dataset.insertErrID(errDF, constants.FILE_UPLOAD_NAME)
            response = jsonify({"Total Row Success":constants.SENT_SUCCESS_EMAIL,"Total Row Error":constants.SENT_ERROR_EMAIL,"message":constants.RESPONSE_MESSAGE, "status_code":HTTPStatus.ACCEPTED})
        else:
            response = jsonify({"message":"File Doesn't Exists", "status_code":HTTPStatus.NOT_FOUND})
        await asyncio.sleep(0.2)
        return response
    def HTMLTemplate():
        html_template = """
            <html>
                <head></head>
                    <body>
                        <h1>Ini adalah template HTML untuk email</h1>
                        <p>Halo, Ini adalah isi email dengan menggunakan HTML.</p>
                        <p>Terima kasih.</p>
                    </body>
            </html>
            """
        return html_template
        
class dataset():
    def loaddata(self, args):
        file_path = constants.PUBLIC_DIR+"/files/"+args['filename']
        file_path = os.path.abspath(file_path)
        df = pd.read_excel(file_path, sheet_name=0)
        df = pd.DataFrame(df)
        return df
    def createDF(arry, colomn):
        columns=colomn
        df = pd.DataFrame(list(zip(arry)), columns=columns)
        return df
    def insertErrID(df, filename):
        file_path = "public/file/"+"Err"+filename
        file_path = os.path.abspath(file_path)
        df.to_excel(file_path, index=False, sheet_name='errEmail')
 #End Of Line Class Logic Function