from flask import Blueprint, jsonify, request
from src.logic import CredentialFunction, File, Email
from werkzeug.utils import secure_filename
import os
import asyncio 
from http import HTTPStatus
from src.constants import constants
import logging






bpAuth = Blueprint('auth', __name__, url_prefix='/auth')
bpFile = Blueprint('filesystem', __name__, url_prefix='/filesystem')
bpEmail = Blueprint('email', __name__, url_prefix='/email')
logging.basicConfig(filename="src/logs/process.txt", level=logging.DEBUG)

#Services Auth
class Auth():
    @bpAuth.route('/outlooksign', methods=['POST'])
    def outlooksign():
        # parser = reqparse.RequestParser()
        # parser.add_argument('email', required=True)
        # parser.add_argument('password', required=True)
        # args = parser.parse_args()
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        args = {"username": username, "password": password, "email": email}
        message  = "\n=========================================================\n"
        message += "         "+str(args)+"          \n"
        message += "=========================================================\n"
        logging.debug(message)
        initLogin = CredentialFunction(args)
        getCreden = initLogin.getCredential()
        return getCreden, HTTPStatus.ACCEPTED
    
    

#Service File
class FileBp():
    @bpFile.route('/upload', methods=['POST'])
    def upload_file():
        # check if the post request has the file part
        if 'filename' not in request.files:
            resp = jsonify({'message' : 'No file part in the request'})
            resp.status_code = 400
        else:
            file = request.files['filename']
        # return resp
            if file.filename == '':
                resp = jsonify({'message' : 'No file selected for uploading'})
                resp.status_code = 400
		        # return resp
            if file and File.allow_file_upload(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(constants.PUBLIC_DIR+"/files", filename))
                resp = jsonify({'message' : 'File successfully uploaded'})
                resp.status_code = 201
		        # return resp
            else:
                resp = jsonify({'message' : 'Allowed file types are txt, pdf, png, jpg, jpeg, gif'})
                resp.status_code = 400
        message  = "\n=========================================================\n"
        message += "         "+str(resp)+"         \n"
        message += "=========================================================\n"
        logging.basicConfig(filename="src/logs/process.txt", level=logging.DEBUG)
        logging.debug(message)
        return resp
#Services File

# class AutomationEmail(Resource):
#     def post(self):
#         parser = reqparse.RequestParser()
#         parser.add_argument('filename', required=True)
#         args = parser.parse_args()
#         initEmail = Email(args)
#         constants.FILE_UPLOAD_NAME=args["filename"]
#         check_file = asyncio.run(initEmail.BulkSentEmail())
#         return make_response(check_file)

#Services AutomationEmail
class BPEmail():
    @bpEmail.route('/bulkautomation', methods=['POST'])
    def AutomationEmail():
        filename = request.form['filename']
        args = {"filename": filename}
        initEmail = Email(args)
        constants.FILE_UPLOAD_NAME=args["filename"]
        check_file = asyncio.run(initEmail.BulkSentEmail())
        return check_file
#Services AutomationEmail